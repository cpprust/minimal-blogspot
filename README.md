# minimal-blogspot

Minimal html code on [blogger](https://www.blogger.com/).

```html
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html xmlns='http://www.w3.org/1999/xhtml' xmlns:b='http://www.google.com/2005/gml/b' xmlns:data='http://www.google.com/2005/gml/data' xmlns:expr='http://www.google.com/2005/gml/expr'>

<head>
  <meta charset="utf-8"/>
</head>
  
<body>
</body>
  
<!-- blogspot requirement -->
<b:skin><![CDATA[]]></b:skin>
<b:section id='b:section'/>
  
</html>
```

To redirect, insert the following script to `<head>`:

```html
<script>window.location.href="your-url";</script>
```
